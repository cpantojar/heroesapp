import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keys'
})
export class KeysPipe implements PipeTransform {
  //valor sera el objeto que tendra las llaves
  transform(value: any): any {
    let keys = [];

    for (let key in value) {
      keys.push(key);
    }
    return keys;
  }

}
